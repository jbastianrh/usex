<HTML>
<A NAME="top"></A>
<HEAD> <TITLE>UNIX System Exerciser</TITLE> </HEAD>

<!-- <BODY TEXT="#000000" BGCOLOR="#FFFFFF" BGCOLOR="F8F8FF"> -->
<BODY BGCOLOR="#FFFFFF" TEXT="000000" LINK="000000" ALINK="00FF00" VLINK="000000">
<!-- base href="http://www.missioncriticallinux.com/~anderson/cas.html" -->
<owner_name="Dave Anderson, anderson@missioncriticallinux.com">
<LINK rev="made" href="mailto:anderson@missioncriticallinux.com">

<!-- <CENTER><IMG SRC="graphics/vspacer.gif" height=10></CENTER> -->
<!-- <CENTER><IMG SRC="graphics/under_construction.gif"></CENTER> -->
<CENTER><IMG SRC="graphics/vspacer.gif" height=10></CENTER>

<CENTER><IMG SRC="graphics/line57.gif"></CENTER>
<CENTER><IMG SRC="graphics/vspacer.gif" height=7"></CENTER>

<TABLE BORDER=0 ALIGN=CENTER WIDTH="90%">
<TR><TD ALIGN=CENTER COLSPAN=3><IMG SRC="graphics/usex_0-110-0.gif"></TD></TR>

<TR>
<TD COLSPAN=3 ALIGN=CENTER><A HREF="options.html"><IMG BORDER=0 SRC="graphics/options_s.jpg"></A>
<IMG SRC="graphics/keyboard.gif">
<A HREF="display.html"><IMG BORDER=0 SRC="graphics/display_s.jpg"></A>
</TD>
</TR>

<TR>
<TD ALIGN=RIGHT NOWRAP>
<IMG BORDER=0 SRC="graphics/vspacer.gif" height=1 width=20>
<A BORDER=0 HREF="builtin.html"><IMG BORDER=0 SRC="graphics/builtin_s.jpg"></A>
</TD>
<TD ALIGN=CENTER NOWRAP>
<IMG BORDER=0 SRC="graphics/vspacer.gif" hight=1 width=15>
<IMG BORDER=0 SRC="graphics/usex_banner.jpg">
</TD>
<TD NOWRAP>
<IMG BORDER=0 SRC="graphics/vspacer.gif" width=5>
<A HREF="usex_tutorial.html"><IMG BORDER=0 SRC="graphics/tutorial_s.jpg"></A>
</TD>
</TR>

<TR>
<TD COLSPAN=3 ALIGN=CENTER NOWRAP>
<A HREF="shell.html"><IMG BORDER=0 SRC="graphics/shell_s.jpg"></A>
<IMG BORDER=0 SRC="graphics/vspacer.gif" height=1 width=65>
<A HREF="interactive.html"><IMG BORDER=0 SRC="graphics/interactive_s.jpg"></A>
</TR>

</TABLE>
<BR>
<CENTER><IMG SRC="graphics/line57.gif"></CENTER>

<table align=center border=0>
<tr>
<td width=760>

<BLOCKQUOTE>
<FONT SIZE="3"><B>
<BR>
The <TT><I>usex</I></TT> UNIX System Exerciser gives its user the capability
of thoroughly exercising and testing several different kernel subsystems.
<TT><I> usex</I></TT> is a single executable that acts as as test harness
controlling one or more test programs.  The test programs can be
selected from a suite of <A HREF="builtin.html">built-in tests</A>, or external
<A HREF="shell.html">user-supplied</A> test programs.  In addition to showing
the current state of each test program, the <TT><I>usex</I></TT>
<A HREF="display.html#display_screen_contents">display screen</A>
continuously displays several key kernel subsystem <A HREF="display.html#statistics">performance statistics</A>.
<P>
The behaviour of a <TT><I>usex</I></TT> session can be invoked interactively
by answering several prompts that specify exactly which tests to run.
Alternatively, an <A HREF="options/dash_i.html">input file</A> may be specified on
the command line that dictates the desired
test suite.  Lastly, there are two common test suite "examples" that can
be kicked off via the <A HREF="options.html">command line</A> options
<A HREF="options/dash_e.html"><TT>usex -e</TT></A> and
<A HREF="options/dash_b.html"><TT>usex -b</TT></A>.
<P>
However, to best understand how to use <TT><I>usex</I></TT> most effectively 
for a given need, check out the accompanying <A HREF="usex_tutorial.html">tutorial</A>,
which steps through the set of prompts issued when 
<TT><I>usex</I></TT> is invoked interactively without an input file.  
Once the capabilities and options are understood, an input file can be created
to easily re-run a desired test suite from a single <A HREF="options.html">command line</A> entry.
<P>
Here is a <A HREF="display.html#display_screen_contents">display screen</A> shot of a <TT><I>usex</I></TT> session running
a set of its <A HREF="builtin.html">built-in tests</A>, consisting of 4 disk
<A HREF="io_test.html">I/O
tests</A>, 1 disk <A HREF="rate.html">transfer rate test</A>, 
1 <A HREF="dhry.html">dhrystone benchmark</A>, 1 <A HREF="whet.html">whetstone
benchmark</A>, 4 <A HREF="vmem.html">virtual memory</A> exercisers, 
and 7 <A HREF="bin.html">bin test suites</A>, each of
which runs well over 300 commands from <TT><I>/bin</I></TT>, 
<TT><I>/usr/bin</I></TT> and <TT><I>/usr/sbin</I></TT>:
<P>
</BLOCKQUOTE>

<TABLE ALIGN=CENTER CELLPADDING=5 BORDER=6 BORDERCOLOR="#0000FF" 
BGCOLOR="#00ffff">
<TH BGCOLOR="#000080" ALIGN=CENTER><FONT COLOR="#ffffff"><TT>usex</TT></TH>
<TR><TD>
<PRE><FONT COLOR="#000080"><B>********************************************************************************
*  ID  BSIZE  MODE   POINTER   OPERATION         FILE NAME         PASS  STAT  *
*  --  -----  ----  ---------  ---------  -----------------------  ----  ----  *
*   1   8192  Fill                        /usr/tmp/ux028425_01        1  BKGD  *
*   2   8192  Fill                        /usr/tmp/ux028425_02        1  BKGD  *
*   3   8192  Fill                        /usr/tmp/ux028425_03        1  BKGD  *
*   4   8192  Fill                        /usr/tmp/ux028425_04        1  BKGD  *
*   5   8192  Rate             read[72%]  /usr/tmp/ux028425_05        3  BKGD  *
*   6  476190 dhrystones/second           dhrystone benchmark        17  BKGD  *
*   7  194.802 MWIPS           Loop N2..  whetstone benchmark         2  BKGD  *
*   8  10 mb random           10.7 mb     virtual memory test            BKGD  *
*   9  10 mb random           10.5 mb     virtual memory test            BKGD  *
*  10  10 mb sequential       19.4 mb     virtual memory test            BKGD  *
*  11  10 mb sequential       19.5 mb     virtual memory test            BKGD  *
*  12  /usr/bin/weave  [                                        ]     1  BKGD  *
*  13  /usr/bin/neqn  [                                         ]     1  BKGD  *
*  14  /usr/bin/guile  [                                        ]     1  BKGD  *
*  15  /usr/bin/wish  [                                         ]     1  BKGD  *
*  16  /usr/bin/ar  [                                           ]     1  BKGD  *
*  17  /usr/bin/unprotoize  [                                   ]     1  BKGD  *
*  18  /usr/bin/rpcinfo  [                                      ]     1  BKGD  *
********************************************************************************
*  Unix System EXerciser  *  USER SYSTEM IDLE  LOADAVG  TASKS/RUN   TEST TIME  *
*    USEX Version 1.7     *   94%    6%    0%    4.47     104/11    000:07:16  *
*                         *                                                    *
*     anderson - i686     *  PAGE-IN PAGE-OUT  FREEMEM  FREESWAP   CSW   INTR  *
*   Linux 2.2.5-15smp2    *    3736    2537     1.1 mb   114 mb    319    378  *
*                         *  SWAP-IN SWAP-OUT  BUFFERS   CACHED   FORKS        *
*     2May01  12:00:58    *     1       47      2.6 mb  27.2 mb     5          *
********************************************************************************
</TABLE> 
<BLOCKQUOTE>
Note that while the set of specified tests are running as indicated in the 
upper box of the <A HREF="display.html#test_data">display screen</A>,
key <A HREF="display.html#statistics">system statistics</A> are updated once per second in the lower part
of the screen. giving the user an excellent indication of how the system
is responding to the stress imposed by the test suite.
<P>
The latest version of <TT><I>usex</I></TT> is: 1.7
<P>
The binary executables are stored on <TT>zaphod</TT>.  On 
machines that are properly configured for automount, the machine-specific
executable is accessible as <I><TT>/nfs/bin/usex</TT></I>:
</BLOCKQUOTE>
<TABLE BGCOLOR="#DDDDDD" ALIGN=CENTER CELLPADDING=5 BORDER=6 >
<TR>
<TH>BINARY LOCATION ON<BR><FONT SIZE="+1"><TT>zaphod</TT></FONT></TH>
<TH>LOCAL AUTOMOUNT<BR>DIRECTORY</TH>
<TH>LOCAL LINK</TH>
</TR>

<TR>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER WIDTH=33%><TT><B>/nfs/i386/bin/usex</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER WIDTH=33%><TT><B>/auto/i386/bin</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER WIDTH=33%><TT><B>/nfs/bin/usex</TD>
</TR>

<TR>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/nfs/alpha/bin/usex</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/auto/alpha/bin</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/nfs/bin/usex</TD>
</TR>

<TR>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/nfs/ppc/bin/usex</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/auto/ppc/bin</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/nfs/bin/usex</TD>
</TR>

<TR>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/nfs/ia64/bin/usex</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/auto/ia64/bin</TD>
<TD BGCOLOR="#EEEEEE" ALIGN=CENTER><TT><B>/nfs/bin/usex</TD>
</TR>
</TABLE>

<BLOCKQUOTE>
Alternatively, <TT><I>usex</I></TT> may be built from source.
The most recent development source code may be
built from the CVS source repository.  If you have automounting enabled on
your workstation, they can be found in:  <I>/nfs/projects/cvsroot/usex</I>.
If not, you can hard mount <I>zaphod:/nfs/projects</I>, and find the sources
in the <I>cvsroot/usex</I> subdirectory:
<BR><BR>
<TABLE ALIGN=CENTER CELLPADDING=20 BORDER=0 BGCOLOR="#00FFFF">
<TR><TD><PRE><B><FONT SIZE="-1" COLOR="#000080">
# export CVSROOT=/nfs/projects/cvsroot
# cvs co usex
cvs checkout: Updating usex
U usex/Makefile
U usex/bin_mgr.c
U usex/curses_mgr.c
U usex/debug.c
U usex/defs.h
U usex/dry.c
U usex/float.c
U usex/gtk_mgr.c
U usex/gtk_mgr.h
U usex/io_test.c
U usex/mk_target_id.c
U usex/shell_mgr.c
U usex/time_mgr.c
U usex/usex.c
U usex/utils.c
U usex/vmem.c
U usex/window_common.c
U usex/window_manager.c
U usex/xfer_mgr.c
# cd usex
# make
cc -c -g -D_CURSES_ usex.c
cc -c -g -D_CURSES_ io_test.c
cc -c -g -D_CURSES_ time_mgr.c
cc -c -g -D_CURSES_ xfer_mgr.c
cc -c -g -D_CURSES_ utils.c
cc -c -g -D_CURSES_ float.c
cc -c -g -D_CURSES_ dry.c
cc -c -g -D_CURSES_ shell_mgr.c
cc -c -g -D_CURSES_ bin_mgr.c
cc -c -g -D_CURSES_ vmem.c
cc -c -g -D_CURSES_ debug.c
cc -c -g -D_CURSES_ window_common.c
cc -c -g -D_CURSES_ window_manager.c
cc  -g build.o usex.o io_test.o time_mgr.o xfer_mgr.o utils.o float.o dry.o shel
l_mgr.o bin_mgr.o vmem.o debug.o window_common.o window_manager.o -o usex -lncur
ses -lm
cc -c -g -D_GTK_ -I/usr/lib/glib/include window_common.c
cc -c -g -D_GTK_ -I/usr/lib/glib/include window_manager.c
cc  -g build.o usex.o io_test.o time_mgr.o xfer_mgr.o utils.o float.o dry.o shel
l_mgr.o bin_mgr.o vmem.o debug.o window_common.o window_manager.o -o gnusex -lm
-I/usr/X11R6/include -I/usr/lib/glib/include -L/usr/lib -L/usr/X11R6/lib -lgtk -
lgdk -rdynamic -lgmodule -lglib -ldl -lXext -lX11 -lm
# usex -v
UNIX System EXerciser (USEX)  Version 1.6
#
</PRE><TD></TR>
</TABLE>
<P>
By default, the <TT><I>usex</I></TT> executable is built dynamically-linked.
To build a statically-linked version of <TT><I>usex</I></TT>,
the <TT>LDFLAGS</TT> definition in the <I>Makefile</I> can be
uncommented:
<BR><BR>
<TABLE ALIGN=CENTER CELLPADDING=20 BORDER=0 BGCOLOR="#00FFFF">
<TR><TD><PRE><B><FONT COLOR="#000080">
# To make usex statically linked uncomment the following LDFLAGS line:
#LDFLAGS=-static
</PRE><TD></TR>
</TABLE>

</td>
</tr>
</table>


<CENTER><IMG SRC="graphics/keyboard.gif"></CENTER>
<CENTER><IMG SRC="graphics/divider.gif"></CENTER>
<CENTER><IMG SRC="graphics/vspacer.gif" height=5></CENTER>
<TABLE ALIGN=CENTER>
<TR ALIGN=CENTER>
<TD NOWRAP COLSPAN=3 ALIGN=CENTER><A BORDER=0 HREF="options.html"><IMG BORDER=0 SRC="graphics/options_s.jpg"></A>
<IMG SRC="graphics/vspacer.gif" width=25>
<A HREF="display.html"><IMG BORDER=0 SRC="graphics/display_s.jpg"></A></TD>
</TR>
<TR>
<TD NOWRAP ALIGN=CENTER><A HREF="builtin.html"><IMG BORDER=0 SRC="graphics/builtin_s.jpg"></A></TD>
<TD NOWRAP><IMG SRC="graphics/vspacer.gif" width=20><A HREF="usex.html"><IMG BORDER=0 SRC="graphics/usex40.jpg"><A></TD>
<TD NOWRAP ALIGN=CENTER><A HREF="usex_tutorial.html"><IMG BORDER=0 SRC="graphics/tutorial_s.jpg"></A></TD>
</TR>
<TR ALIGN=CENTER>
<TD NOWRAP COLSPAN=3 ALIGN=CENTER><A HREF="shell.html"><IMG BORDER=0 SRC="graphics/shell_s.jpg"></A>
<IMG SRC="graphics/vspacer.gif" width=20>
<A HREF="interactive.html"><IMG BORDER=0 SRC="graphics/interactive_s.jpg"></A></TD>
</TR>
</TABLE>
<CENTER><IMG SRC="graphics/divider.gif"></CENTER>
</BODY>
