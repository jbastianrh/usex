#!/bin/sh

OLD_VERSION="1.6"
NEW_VERSION="1.7"

OLD_SCREEN_VERSION=" Version $OLD_VERSION "
NEW_SCREEN_VERSION=" Version $NEW_VERSION "

OLD_LATEST=" is: $OLD_VERSION"
NEW_LATEST=" is: $NEW_VERSION"

#echo OLD_SCREEN_VERSION: [$OLD_SCREEN_VERSION]
#echo NEW_SCREEN_VERSION: [$NEW_SCREEN_VERSION]
#echo OLD_LATEST: [$OLD_LATEST]
#echo NEW_LATEST: [$NEW_LATEST]

update_version() {
  grep "$OLD_SCREEN_VERSION" $1 /dev/null

ed $1 <<!
1,\$ s/$OLD_SCREEN_VERSION/$NEW_SCREEN_VERSION/
w
q
!

  if [ $1 = "usex.html" ]
  then

ed $1 <<!
1,\$ s/$OLD_LATEST/$NEW_LATEST/
w
q
!

  fi

}

for FILE in `/bin/ls *.html options/*.html runtime/*.html`
do
  echo $FILE
  update_version $FILE
done

