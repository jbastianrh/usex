#!/usr/bin/perl

use CGI qw(param);

print "Content-type: text/html\n\n";

print <<"EOF";
<HTML><HEAD><TITLE>USEX Mailing List</TITLE></HEAD></HTML><BODY>
<BODY BGCOLOR="FFFFFF" TEXT="000000" LINK="00FF00" ALINK="00FF00" VLINK="00FF00">
<CENTER><A HREF="../usex.html"><IMG BORDER=0 SRC="../graphics/usex_0-110-0.gif"></A></CENTER>
<CENTER><IMG SRC="../graphics/mailing_list.jpg"></CENTER>
EOF

$ADDRESS= param("email");
print <<"EOF";
<BLOCKQUOTE>
<B>Thank you: <TT>$ADDRESS</TT>. 
<P>
You will be notified when the next release is available.</B>
<BLOCKQUOTE>
EOF

print <<"EOF";
<CENTER><IMG SRC="../graphics/keyboard.gif"></CENTER>
<CENTER><IMG SRC="../graphics/divider.gif"></CENTER>
<CENTER><IMG SRC="../../graphics/vspacer.gif" height=5></CENTER>
<TABLE ALIGN=CENTER>
<TR ALIGN=CENTER>
<TD NOWRAP COLSPAN=3 ALIGN=CENTER><A BORDER=0 HREF="../options.html"><IMG BORDER=0 SRC="../graphics/options_s.jpg"></A>
<IMG SRC="../../graphics/vspacer.gif" width=25>
<A HREF="../display.html"><IMG BORDER=0 SRC="../graphics/display_s.jpg"></A></TD>
</TR>
<TR>
<TD NOWRAP ALIGN=CENTER><A HREF="../builtin.html"><IMG BORDER=0 SRC="../graphics/builtin_s.jpg"></A></TD>
<TD NOWRAP><IMG SRC="../../graphics/vspacer.gif" width=20><A HREF="../usex.html"><IMG BORDER=0 SRC="../graphics/usex40.jpg"><A></TD>
<TD NOWRAP ALIGN=CENTER><A HREF="../usex_tutorial.html"><IMG BORDER=0 SRC="../graphics/tutorial_s.jpg"></A></TD>
</TR>
<TR ALIGN=CENTER>
<TD NOWRAP COLSPAN=3 ALIGN=CENTER><A HREF="../shell.html"><IMG BORDER=0 SRC="../graphics/shell_s.jpg"></A>
<IMG SRC="../../graphics/vspacer.gif" width=20>
<A HREF="../interactive.html"><IMG BORDER=0 SRC="../graphics/interactive_s.jpg"></A></TD>
</TR>
</TABLE>
<CENTER><IMG SRC="../graphics/divider.gif"></CENTER>
</BODY>
EOF

open(MAIL, "|mail -s \"USEX MAILING LIST ADDITION\" anderson\@mclinux.com");
print MAIL "submitted address: $ADDRESS \n";
close(MAIL);
